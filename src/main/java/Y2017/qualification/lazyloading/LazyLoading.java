package Y2017.qualification.lazyloading;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by aatmapremaarya on 08/01/17.
 */
public class LazyLoading {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testcases = in.nextInt();
        for (int i = 0; i < testcases; i++) {
            int noOfvalues = in.nextInt();
            ArrayList<Integer> arrayList = new ArrayList(noOfvalues);
            for (int j = 0; j < noOfvalues; j++) {
                arrayList.add(in.nextInt());
            }
            System.out.println("Case #" + (i + 1) + ": " + getMaximumRound(arrayList));
        }
    }

    public static int getMaximumRound(List<Integer> input) {
        Collections.sort(input);

        int rounds = 0;
        int start = 0;
        int end = input.size() - 1;

        while (start < input.size() && end >= 0 && start <= end) {
            int noOfItmes = 50 / input.get(end) + (50 % input.get(end) == 0 ? 0 : 1);

            if( (end - start + 1) < noOfItmes){
                break;
            }

            end--;
            start += noOfItmes - 1;
            rounds++;
        }

        return rounds;
    }

//    public static void main(String[] args) {
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{50})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{55})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{1,50})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{1,2,50})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{1,2,3,4,5,50})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{30,2,3,4,5,50})));
//        System.out.println(getMaximumRound(Arrays.asList(new Integer[]{30,2,3,4,20,50})));
//
//    }
}
