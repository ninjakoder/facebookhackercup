package Y2017.qualification.progresspie;

import java.util.Scanner;

/**
 * Created by aatmapremaarya on 07/01/17.
 */
public class ProgressPie {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int testcases = in.nextInt();
        for (int i = 0; i < testcases; i++) {
            System.out.println("Case #" + (i + 1) + ": " + (isPointInsidePie(in.nextInt(), in.nextInt() - 50, in.nextInt() - 50) ? "black" : "white"));
        }
    }

    private static boolean isPointInsidePie(int percentage, int x, int y) {
        return isInsideCircle(x, y, 25) && isPointCoveredUnderPercantage(percentage, x, y);
    }

    public static boolean isPointCoveredUnderPercantage(int percentage, int x, int y) {
        return (percentage == 100 || ((percentage * 3.6) > getDegreeFromXY(x, y)));
    }

    private static boolean isInsideCircle(int x, int y, int r) {
        int absX = Math.abs(x);
        int absY = Math.abs(y);

        if ((absX + absY) <= r) {
            return true;
        }

        if (absX > r || absY > r) {
            return false;
        }

        return Math.pow(absX, 2) + Math.pow(absY, 2) < 2500;
    }

    private static double getDegreeFromXY(int x, int y) {
        double degrees = Math.toDegrees(Math.atan2(x, y));
        return degrees < 0 ? 360 + degrees : degrees;
    }

}
