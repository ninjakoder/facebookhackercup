package Y2017.qualification;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;

import org.apache.commons.io.FileUtils;

import Y2017.qualification.lazyloading.LazyLoading;

/**
 * Created by aatmapremaarya on 08/01/17.
 */
public class Driver {

    File inputFile;
    File outputFile;
    File expectedOutputFile;
    PrintStream printStream;

    public Driver(String inputFilePath, String outputFilePath, String expectedOutputFilePath) throws IOException {
        this.inputFile = new File(this.getClass().getClassLoader().getResource(inputFilePath).getFile());

        File tempOutputFile = new File(outputFilePath);
        Files.deleteIfExists(tempOutputFile.toPath());
        tempOutputFile.createNewFile();

        this.outputFile = tempOutputFile;

        if(expectedOutputFilePath != null){
            this.expectedOutputFile = new File(this.getClass().getClassLoader().getResource(expectedOutputFilePath).getFile());
        }

    }

    public static void main(String[] args) throws IOException {
        String inputfile = "lazy_loading_example_input.txt";
        String generatedOutputfile = "lazy_loading_example_generated.txt";
        String expectedOutputfile = "lazy_loading_example_output.txt";

        Driver driver = new Driver(inputfile,generatedOutputfile,expectedOutputfile);
        driver.redirectSysInOut();

        String[] dummyArgs = new String[0];

        LazyLoading lazyLoading = new LazyLoading();
        lazyLoading.main(dummyArgs);

        driver.flushOutput();
        driver.assertAcutalAndExpectedOutputEqual();
    }

    public void redirectSysInOut() throws IOException {
        System.setIn(new BufferedInputStream(new FileInputStream(inputFile)));

        printStream = new PrintStream(new BufferedOutputStream(new FileOutputStream(outputFile)));
        System.setOut(printStream);
    }

    public void flushOutput(){
        printStream.flush();
        printStream.close();
    }

    public void assertAcutalAndExpectedOutputEqual() throws IOException {
        if (expectedOutputFile != null) {
            if(!FileUtils.contentEquals(outputFile,expectedOutputFile)){
               throw new RuntimeException("Files not same");
            }
        }
    }



}
